odoo.define('aspl_pos_report_ee.ProductSummaryPopup', function(require) {
    'use strict';

    const { useState, useRef } = owl.hooks;
    const AbstractAwaitablePopup = require('point_of_sale.AbstractAwaitablePopup');
    const Registries = require('point_of_sale.Registries');

    class ProductSummaryPopup extends AbstractAwaitablePopup {
        onInputKeyDownNumberValidation(e) {
            if(e.which != 190 && e.which != 110 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105) && (e.which < 37 || e.which > 40)) {
                e.preventDefault();
            }
        }
        constructor() {
            super(...arguments);
            if (this.env.pos.config.product_current_month_date){
                var first_date_of_month = moment().startOf('month').format('YYYY-MM-DD');
                var today_date = moment().format('YYYY-MM-DD');
            }
            this.state = useState({
                StartDate: first_date_of_month || false,
                EndDate: today_date || false,
                StartDateBlank:false,
                EndDateBlank:false,
                ProdNumberReceipt: this.env.pos.config.no_of_product_receipt || 1,
            });
            this.start_date = useRef('start_date');
        }
        getPayload() {
            return {
                StartDate: this.state.StartDate,
                EndDate: this.state.EndDate,
                CurrentSession: this.state.CurrentSession,
                ProductSummary: this.state.ProductSummary,
                CategorySummary: this.state.CategorySummary,
                LocationSummary: this.state.LocationSummary,
                PaymentSummary: this.state.PaymentSummary,
                ProdNumberReceipt: this.state.ProdNumberReceipt,
            };
        }
        ProductSummaryCheck(){
            this.state.ProductSummary = !this.state.ProductSummary;
        }
        CategorySummaryCheck(){
            this.state.CategorySummary = !this.state.CategorySummary;
        }
        PaymentSummaryCheck(){
            this.state.PaymentSummary = !this.state.PaymentSummary;
        }
        LocationSummaryCheck(){
            this.state.LocationSummary = !this.state.LocationSummary;
        }
        CurrentSessionCheck(){
            this.state.CurrentSession = !this.state.CurrentSession;
        }
        async confirm() {
            if(this.state.ProdNumberReceipt <= 0){
                 $('#no_of_summary').css('border','1px solid red');
                 return
            }
            if (!this.state.CurrentSession){
                if (this.state.StartDate == ""){
                    this.state.StartDateBlank = true;
                }
                if (this.state.EndDate == ""){
                    this.state.EndDateBlank = true;
                }
                if (this.state.StartDateBlank || this.state.EndDateBlank){
                    return
                } else if(this.state.StartDate > this.state.EndDate){
                    $('#lbl_set_available').html("Start date should not be greater than end date !");
                    return
                } else{
                    this.props.resolve({ confirmed: true, payload: await this.getPayload() });
                    this.trigger('close-popup');
                }
            } else{
                this.props.resolve({ confirmed: true, payload: await this.getPayload() });
                this.trigger('close-popup');
            }
        }
        cancel() {
            this.trigger('close-popup');
        }
    }
    ProductSummaryPopup.template = 'ProductSummaryPopup';
    ProductSummaryPopup.defaultProps = {
        confirmText: 'Print',
        cancelText: 'Cancel',
        title: '',
        body: '',
    };

    Registries.Component.add(ProductSummaryPopup);

    return ProductSummaryPopup;
});
