odoo.define('aspl_pos_report_ee.OrderSummaryButton', function(require) {
    'use strict';

    var core = require('web.core');
    var Printer = require('point_of_sale.Printer').PrinterMixin;
    var QWeb = core.qweb;
    var rpc = require('web.rpc');

    const PosComponent = require('point_of_sale.PosComponent');
    const ProductScreen = require('point_of_sale.ProductScreen');
    const { useListener } = require('web.custom_hooks');
    const Registries = require('point_of_sale.Registries');

    class OrderSummaryButton extends PosComponent {
        constructor() {
            super(...arguments);
            useListener('click', this.onClick);
        }
        async printReceipt(report){
            this.env.pos.proxy.printer.print_receipt(report);
        }
        async getReportData(val){
            return this.rpc({
                model: 'pos.order',
                method: 'order_summary_report',
                args: [val]
            });
        }
        async onClick() {
            const { confirmed, payload } = await this.showPopup('OrderSummaryPopup', {
                title: this.env._t('Order Summary'),
            });

            if (confirmed) {
                let order = this.env.pos.get_order();
                var empty_array = {}
                let summaryObject = {};
                let reportValueList = [];
                const {CategorySummary, CurrentSession, EndDate, OrderNumberReceipt,
                        OrderSelectType, OrderSummary, PaymentSummary, StartDate} = payload;

                order.set_number_of_print(Number(payload.OrderNumberReceipt) || 1)

                OrderSummary    ? reportValueList.push('order_summary_report') : '';
                PaymentSummary  ? reportValueList.push('payment_summary_report') : '';
                CategorySummary ? reportValueList.push('category_summary_report') : '';

                if(CurrentSession){
                    Object.assign(summaryObject, { state: OrderSelectType, summary: reportValueList,
                                                  session_id:  this.env.pos.pos_session.id });
                    this.env.pos.order_print_date = false;
                } else {
                    Object.assign(summaryObject, { start_date: StartDate, end_date: EndDate,
                                                  state: OrderSelectType, summary: reportValueList });
                    this.env.pos.order_print_date = true;
                    this.env.pos.order_date = [ payload['StartDate'], payload['EndDate'] ];
                }
                let data = await this.getReportData(summaryObject);
                const {orderReport, categoryReport, paymentReport, State} = data;

                if(!orderReport && !paymentReport){
                    alert("No records found!");
                    return;
                }
                if(!State){
                    this.env.pos.state = false;
                } else{
                    this.env.pos.state = true;
                }
                if (orderReport == empty_array){
                    orderReport = false
                }
                console.log("////////******--------*****///////",orderReport)
                if (this.env.pos.config.iface_print_via_proxy || this.env.pos.config.epson_printer_ip) {
                    const report = this.env.qweb.renderToString('OrderSummaryReceipt',{
                        props: {
                            'CompanyData': order.getOrderReceiptEnv().receipt,
                            'OrderReportData': orderReport,
                            'CategoryReportData': categoryReport,
                            'PaymentReportData': paymentReport,
                        }, env : this.env
                    });
                    const rec = Array(Number(payload.OrderNumberReceipt) || 1).fill(report)
                    const printResult = await this.env.pos.proxy.printer.print_receipt(rec);
                    if (!printResult.successful) {
                        await this.showPopup('ErrorPopup', {
                            title: printResult.message.title,
                            body: printResult.message.body,
                        });
                    }
                } else{
                    this.showScreen('ReceiptScreen', {'check':'from_order_summary',
                        'CompanyData': order.getOrderReceiptEnv().receipt,
                        'OrderReportData': orderReport,
                        'CategoryReportData': categoryReport,
                        'PaymentReportData': paymentReport,
                    });
                }
            }
        }
    }
    OrderSummaryButton.template = 'OrderSummaryButton';

    ProductScreen.addControlButton({
        component: OrderSummaryButton,
        condition: function() {
            return this.env.pos.config.enable_order_summary;
        },
    });

    Registries.Component.add(OrderSummaryButton);

    return OrderSummaryButton;

});
